<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.9";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.9</h1>
<pre>
Summary           : Multiple vulnerabilities fixed in VLC media player
Date              : April 2020
Affected versions : VLC media player 3.0.0 to 3.0.8 for most issues
ID                : VideoLAN-SB-VLC-309
CVE references    : CVE-2019-19721 CVE-2020-6071 CVE-2020-6072 CVE-2020-6073 CVE-2020-6077 CVE-2020-6078 CVE-2020-6079
</pre>

<h2>Details</h2>
<p>A remote user could:</p>
<p>- Create a specifically crafted image file that could trigger an out of bounds read</p>
<p>- Send a specifically crafter request to the microdns service discovery, potentially triggering various memory management issues</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or 
remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through these vulnerabilities</p>
<br />
<p>CVE-2019-19721 affects VLC 3.0.8 and earlier, and only reads 1 byte out of bound</p>

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>
<p>Until VLC gets updated to the 3.0.9 version, it is advised not to use the network discovery, including for chromecast</p>

<h2>VLC-iOS</h2>
<p>VLC on iOS suffered from an access to restricted folders <em>(passcode feature)</em> under the web interface without entering the passcode.</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.9</b> addresses the issues. The Android versions <b>3.2.12</b> and iOS <b>3.2.7</b> are addressing those issues too.
</p>

<h2>Credits</h2>
<p>CVE-2019-19721 was reported by Antonio Morales from the Semmle Security Team.</p>
<p>CVE-2020-6071 CVE-2020-6072 CVE-2020-6073 CVE-2020-6077 CVE-2020-6078 CVE-2020-6079 were discovered &amp; reported by Claudio Bozzato from Cisco Talos.</p>
<p>VLC iOS web interface issue was reported by Dhiraj Mishra.</p>

<h2>Special thanks</h2>
<p>VideoLAN would like to thank Antonio Morales and Claudio Bozzato for their time and cooperation with the VideoLAN security team</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
<dt>VLC-iOS official GIT repository</dt>
<dd><a href="https://code.videolan.org/videolan/vlc-ios">https://code.videolan.org/videolan/vlc-ios</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
