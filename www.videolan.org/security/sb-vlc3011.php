<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.11";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.11</h1>
<pre>
Summary           : Multiple vulnerabilities fixed in VLC media player
Date              : June 2020
Affected versions : VLC media player 3.0.10 and earlier
ID                : VideoLAN-SB-VLC-3011
CVE references    : CVE-2020-13428
</pre>

<h2>Details</h2>
<p>A remote user could create a specifically crafted file that could trigger a buffer overflow in VLC's H26X packetizer</p>

<h2>Impact</h2>
<p>The affected code was only used by macOS/iOS hardware accelerated decoder (VideoToolbox), meaning other platforms are unaffected.</p>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or 
remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through these vulnerability</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.11</b> addresses the issue.
</p>

<h2>Credits</h2>
<p>CVE-2020-13428 was reported by Tommy Muir</p>

<h2>Additional notes</h2>
<p>VLC 3.0.11 also bumps some dependencies, notably libarchive, following the publication of CVE-2020-9308 and CVE-2019-19221</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
