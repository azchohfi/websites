# Estonian translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Mihkel Tõnnov <mihhkel@gmail.com>, 2013-2014
# Ildar Kamaev <thetechnorat@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-10-06 14:28+0200\n"
"Last-Translator: VideoLAN <videolan@videolan.org>, 2017\n"
"Language-Team: Estonian (http://www.transifex.com/yaron/vlc-trans/language/"
"et/)\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr ""

#: include/header.php:289
msgid "non-profit organization"
msgstr "mittetulundusühing"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partnerid"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr ""

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr ""

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Sündmused"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Juriidiline info"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Pressiüksus"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontakt"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Allalaadimine"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funktsioonid"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr ""

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Vahva kraam"

#: include/menus.php:51
msgid "Projects"
msgstr "Projektid"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Kõik projektid"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Aita kaasa"

#: include/menus.php:77
msgid "Getting started"
msgstr ""

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr ""

#: include/menus.php:79
msgid "Report a bug"
msgstr "Teata veast"

#: include/menus.php:83
msgid "Support"
msgstr "Tugi"

#: include/footer.php:33
msgid "Skins"
msgstr "Kestad"

#: include/footer.php:34
msgid "Extensions"
msgstr "Laiendused"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Kuvapildid"

#: include/footer.php:61
msgid "Community"
msgstr "Kogukond"

#: include/footer.php:64
msgid "Forums"
msgstr "Foorumid"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Postiloendid"

#: include/footer.php:66
msgid "FAQ"
msgstr "KKK"

#: include/footer.php:67
msgid "Donate money"
msgstr "Anneta raha"

#: include/footer.php:68
msgid "Donate time"
msgstr "Anneta aega"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projekt ja organisatsioon"

#: include/footer.php:76
msgid "Team"
msgstr "Meeskond"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Peeglid"

#: include/footer.php:83
msgid "Security center"
msgstr "Turvakeskus"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Aita kaasa"

#: include/footer.php:85
msgid "News"
msgstr "Uudised"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Laadi alla VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Teised süsteemid"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr ""

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC on tasuta ja avatud lähtekoodiga mitmeplatvormne multimeediamängija ja "
"raamistik, mis esitab nii enamikku multimeediafaile kui ka DVD-sid, audio-CD-"
"sid, VCD-sid ja toetab mitmeid voogedastusprotokolle."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC ametlik koduleht - Vabad multimeedialahendused kõigile!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Teised VideoLAN-i projektid"

#: index.php:30
msgid "For Everyone"
msgstr "Kõigile"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC on võimas meediamängija, mis oskab esitada enamikku olemasolevatest "
"meediakoodekitest ja videovormingutest."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator on mittelineaarne videotöötlustarkvara."

#: index.php:62
msgid "For Professionals"
msgstr "Professionaalidele"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast on lihtne ja võimas MPEG-2/TS-i demuksimis- ja voogedastusrakendus."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat on komplekt tööriistu multicast-voogude ja TS-i lihtsaks ja "
"tõhusaks töötlemiseks."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 on tasuta rakendus videovoogude kodeerimiseks H.264/MPEG-4 AVC "
"vormingusse."

#: index.php:104
msgid "For Developers"
msgstr "Arendajatele"

#: index.php:140
msgid "View All Projects"
msgstr "Vaata kõiki projekte"

#: index.php:144
msgid "Help us out!"
msgstr "Tule appi!"

#: index.php:148
msgid "donate"
msgstr "anneta"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN on mittetulunduslik organisatsioon."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
" Kõik meie kulutused tuleb katta kasutajate annetustest. Kui naudid mõne "
"VideoLANi toote kasutamist, siis palun anneta meile."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Loe edasi"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN on avatud lähtekoodiga tarkvara."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"See tähendab, et kui sul on oskusi ja tahtmist mõnda meie toodet paremaks "
"teha, oled teretulnud kaasa lööma."

#: index.php:187
msgid "Spread the Word"
msgstr "Levita sõna"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Leiame, et VideoLANil on parim videotarkvara parima hinnaga: tasuta. Kui "
"arvad samuti, aita palun aita kaasa selle tarkvara levitamisele."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Uudised &amp; uuendused"

#: index.php:218
msgid "More News"
msgstr ""

#: index.php:222
msgid "Development Blogs"
msgstr "Arendusblogid"

#: index.php:251
msgid "Social media"
msgstr "Sotsiaalmeedia"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "VLC opsüsteemile"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr ""

#: vlc/index.php:35
msgid "Plays everything"
msgstr ""

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr ""

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr ""

#: vlc/index.php:44
msgid "Completely Free"
msgstr ""

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""

#: vlc/index.php:47
msgid "learn more"
msgstr ""

#: vlc/index.php:66
msgid "Add"
msgstr ""

#: vlc/index.php:66
msgid "skins"
msgstr ""

#: vlc/index.php:69
msgid "Create skins with"
msgstr ""

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr ""

#: vlc/index.php:72
msgid "Install"
msgstr ""

#: vlc/index.php:72
msgid "extensions"
msgstr ""

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Vaata kõiki kuvapilte"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "VLC meediamängija ametlikud allalaadimised"

#: vlc/index.php:146
msgid "Sources"
msgstr "Lähtekood"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Võid alla laadida ka "

#: vlc/index.php:148
msgid "source code"
msgstr "lähtekoodi"

#~ msgid "A project and a"
#~ msgstr "Projekt ja"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "mis koosneb vabatahtlikest, kes arendavad ja edendavad vabavaralisi "
#~ "multimeedia lahendusi."

#~ msgid "why?"
#~ msgstr "miks?"

#~ msgid "Home"
#~ msgstr "Esileht"

#~ msgid "Support center"
#~ msgstr "Tugikeskus"

#~ msgid "Dev' Zone"
#~ msgstr "Arendajate ala"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Lihtne, kiire ja võimas meediamängija."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Esitab kõike: faile, plaate, veebikaameraid, seadmeid ja vooge."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Esitab ilma koodekipakita enamikku koodekeid:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Töötab kõigil platvormidel:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Täiesti tasuta, ei mingit nuhkvara, ei mingeid reklaame ja ei mingit "
#~ "kasutaja jälgimist."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Oskab multimeediat teisendada ja voogedastada."

#~ msgid "Discover all features"
#~ msgstr "Vaata kõiki funktsioone"

#~ msgid "DONATE"
#~ msgstr "ANNETA"

#~ msgid "Other Systems and Versions"
#~ msgstr "Teised süsteemid ja versioonid"

#~ msgid "Other OS"
#~ msgstr "Muud opsüsteemid"
